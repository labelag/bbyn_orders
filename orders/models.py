from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import HStoreField,ArrayField,JSONField
from django.utils import timezone
from django.core.urlresolvers import reverse

# Create your models here.
class Restaurant(models.Model):
    name = models.CharField(max_length=255)

    def get_absolute_url(self):
        return reverse('restaurnat-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name

class Schedule(models.Model):
    restaurant = models.ForeignKey(Restaurant)
    schedule_date = models.DateField(unique=True)
    default =  models.BooleanField(default=False)

    def __str__(self):
        return self.restaurant.name

def get_TodayScheduleID():
    today = timezone.datetime.today()
    result = Schedule.objects.filter(schedule_date__year=today.year, schedule_date__month=today.month,
                                     schedule_date__day=today.day)
    if result.count() > 0:
        return result[0].pk
    else:
        if Schedule.objects.filter(default=True).count() > 0:
            return Schedule.objects.filter(default=True)[0].pk
        else:
            raise Exception("Error No Default Schedule")



class Orders(models.Model):
    date = models.DateField(auto_now_add=True)
    schedule = models.ForeignKey(Schedule,default=get_TodayScheduleID())
    items = JSONField(verbose_name="Items",default={})
    def get_absolute_url(self):
        return reverse('order-detail', kwargs={'pk': self.pk})




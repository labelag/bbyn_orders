from django.shortcuts import render,HttpResponseRedirect
from django.utils import timezone
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required
from . import models,forms
# Create your views here.
class HomeStatic(TemplateView):
    template_name = "index.html"


@login_required
def TodayOrder(request):
    myorder = None
    success = None
    today = timezone.datetime.today()
    order,created = models.Orders.objects.get_or_create(date__year=today.year,date__month=today.month,date__day=today.day)
    if request.method == 'POST':
        mydict = {}
        mydict['order'] = request.POST.get('items')
        mydict['name'] = request.user.get_full_name()
        order.items[request.user.username]= mydict
        order.save()
        success = True

    if order.items.has_key(request.user.username):
        myorder = order.items.get(request.user.username)
    else:
        myorder = ''

    return render(request, 'order.html', {"order": order, "myorder":myorder,"success":success},
            content_type="text/html")

@login_required
def ScheduleView(request,pk=None):
    if request.method == "POST":
        myform = forms.ScheduleForms(request.POST)

        if myform.is_valid():
            myform.save()
            return HttpResponseRedirect(reverse_lazy('list-schedule'))

    else:
        if pk:
            myform = forms.ScheduleForms(instance=models.Schedule.objects.get(pk=pk))
        else:
            myform = forms.ScheduleForms()
    return render(request, 'schedule.html', {
             'form': myform,
            })



class RestaurantCreate(SuccessMessageMixin,CreateView):
    model = models.Restaurant
    success_message = "%(name)s was created successfully"
    fields = ['name']
    success_url = reverse_lazy('list-restaurant')


class RestaurantUpdate(SuccessMessageMixin,UpdateView):
    model = models.Restaurant
    fields = ['name']
    success_message = "%(name)s was updated successfully"
    success_url = reverse_lazy('list-restaurant')

class ScheduleUpdate(SuccessMessageMixin,UpdateView):
    model = models.Schedule
    fields = ['restaurant','schedule_date','default']
    success_message = "%(schedule_date)s was updated successfully"
    success_url = reverse_lazy('list-schedule')

class RestaurantDelete(DeleteView):
    model = models.Restaurant
    success_url = reverse_lazy('list-restaurant')

class ScheduleDelete(DeleteView):
    model = models.Schedule
    success_url = reverse_lazy('list-schedule')

class RestaurantList(ListView):
    model = models.Restaurant

class ScheduleList(ListView):
    model= models.Schedule


class RestaurantDetail(DetailView):
    model = models.Restaurant

class OrderDetail(DetailView):
    model = models.Orders


def TodayOrderReport(request):
    today = timezone.datetime.today()
    order,created = models.Orders.objects.get_or_create(date__year=today.year,date__month=today.month,date__day=today.day)
    return HttpResponseRedirect(reverse_lazy('order-detail', kwargs={'pk':order.pk}))
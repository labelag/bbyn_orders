from django import forms
from . import models


class ScheduleForms(forms.ModelForm):

    class Meta:
        model = models.Schedule
        widgets = {
            'schedule_date': forms.DateInput(attrs={'class':'datepicker'}),
        }
        fields = '__all__'



class RestaurantForms(forms.ModelForm):


    class Meta:
        model = models.Restaurant
        fields=['name',]
"""BBYN URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin,staticfiles
from django.contrib.auth.views import login,logout
from django.conf import settings

from orders.views import HomeStatic, TodayOrder, ScheduleView, RestaurantCreate, RestaurantDelete, RestaurantUpdate, \
    RestaurantDetail, RestaurantList, ScheduleList, ScheduleDelete,ScheduleUpdate, OrderDetail,TodayOrderReport

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
]



urlpatterns += [
        url(r'^accounts/home/$', HomeStatic.as_view(), name='home'),

    url(r'^home/$', HomeStatic.as_view(), name='home'),
     url(r'^order/$', TodayOrder, name='order'),
url(r'^schedule/add/$', ScheduleView, name='add-schedule'),
  url(r'^schedule/delete/(?P<pk>[0-9]+)/$', ScheduleDelete.as_view(), name='delete-schedule'),
  url(r'^schedule/edit/(?P<pk>[0-9]+)/$', ScheduleUpdate.as_view(), name='edit-schedule'),

    url(r'^schedule/$', ScheduleList.as_view(), name='list-schedule'),

url(r'^restaurant/add/$', RestaurantCreate.as_view(), name='add-restaurant'),
    url(r'^restaurant/edit/(?P<pk>[0-9]+)/$', RestaurantUpdate.as_view(), name='edit-restaurant'),
url(r'^restaurant/delete/(?P<pk>[0-9]+)/$', RestaurantDelete.as_view(), name='delete-restaurant'),
url(r'^restaurant/detail/(?P<pk>[0-9]+)/$', RestaurantDetail.as_view(), name='restaurnat-detail'),
    url(r'^order/today/$', TodayOrderReport, name='order-today'),
   url(r'^order/detail/(?P<pk>[0-9]+)/$', OrderDetail.as_view(), name='order-detail'),
    url(r'^restaurant/$', RestaurantList.as_view(), name='list-restaurant'),



    url(r'^login/$', login,name="login"),
    url(r'^logout/$', logout,{'next_page': '/accounts/home/'},name="logout"),
        url(r'^$', HomeStatic.as_view(), name='home'),

]

# if settings.DEBUG:
#     urlpatterns += [
#         url(r'^static/(?P<path>.*)$', staticfiles.views.serve),
#     ]